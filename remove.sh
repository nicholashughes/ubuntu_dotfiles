#!/usr/bin/env bash

remove_symlinks() {
    echo "Removing dotfile symlinks"

    # Get the location of this script, no matter what directory it was run from
    script_loc=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # Loop over dotfiles and unsymlink them
    for f in $script_loc/dotfiles/*; do
        link_file="$HOME/.${f##*/}"

        if [[ $f == $script_loc/dotfiles/'.' ]] ||
           [[ $f == $script_loc/dotfiles/'..' ]]; then
            :
        else
            # If the link file is a symlink, unlink it. Unlink will remove a regular
            # file so we don't want it to remove dotfiles that are not symlinks
            test -h "$link_file" && unlink "$link_file" && echo "Unlinking $link_file"
        fi
    done
}

remove_symlinks

