#!/usr/bin/env bash

set -e

# Create some variables that all the functions need: 'time', 'backup_loc', 'backup_dir'
time=$(date +%s)
backup_loc="$HOME"/.dotfiles_backups
if [ -d "$backup_loc/orig" ]; then
    backup_dir="$backup_loc/$time"
else
    backup_dir="$backup_loc/orig"
fi

create_backup_directory() {
    # Create backup directory in $HOME/.dotfiles_backups/<orig or time>
    mkdir -p "$backup_dir"

    # If we ran this script as 'sudo', then reset the owner and group
    local current_user
    current_user=$(who am i | awk '{print $1}')
    chown -R "${current_user}" "${backup_loc}"
    chgrp -R "${current_user}" "${backup_loc}"
}

remove_empty_backup() {
    # If something was moved into the backup directory, do nothing
    if [ "$(ls -A "$backup_dir")" ]; then
         : # noop
    # Else, if the backup directory is empty, ie we put nothing in it, then delete it
    else
        rm -rf "$backup_dir"
    fi
}

create_symlinks() {
    if ! [ -d "$backup_dir" ]; then
        echo "No backup directory defined"
        exit 1
    fi

    printf "\nCreating dotfile symlinks\n"
    # Get the location of this script, no matter what directory it was run from
    script_loc=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
    # Loop over dotfiles and symlink them
    for f in $script_loc/dotfiles/*; do
        if [[ $f == $script_loc/'.git' ]] ||
           [[ $f == $script_loc/'.' ]] ||
           [[ $f == $script_loc/'..' ]] ||
           [[ ${f:(-1)} == '~' ]]; then
            :
        else
            original_file="$HOME/.${f##*/}"

            # If $original_file exists and is the same as $f, do nothing
            if test -e "$original_file" && diff "$f" "$original_file" > /dev/null; then
                echo "Dotfile $(basename ${f}) has not changed; leaving current symlink"
            # Else they are not same, so move the original dotfile to the backup dir and then
            # create a symlink to the new version
            else
                test -e "$original_file" && mv "$original_file" "$backup_dir"
                ln -s "$f" "$original_file"
                echo "Linking $original_file"
            fi
        fi
    done
}

# Taken from Docker install script
get_distribution() {
	lsb_dist=""
	# Every system that we officially support has /etc/os-release
	if [ -r /etc/os-release ]; then
		lsb_dist="$(. /etc/os-release && echo "$ID")"
	fi
	# Returning an empty string here should be alright since the
	# case statements don't act unless you provide an actual value
	echo "$lsb_dist"
}

# The software needed for the dotfiles, see bash_functions, gitattributes, screenrc, etc
printf "\nInstalling dependencies\n"
lsb_dist=$( get_distribution )
lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
dist_array=("ubuntu debian")
for value in "${dist_array[@]}"
do
    if [ "${value}" == "${lsb_dist}" ]; then
        ubuntu_or_debian=true
        break
    else
        ubuntu_or_debian=false
    fi
done

# Add the git core PPA
if [ "${ubuntu_or_debian}" == true ]; then
    sudo apt-add-repository -y ppa:git-core/ppa
fi

sudo apt-get -qq update
sudo apt-get install -y -qq catdoc dnsutils docx2txt gdb git groff libimage-exiftool-perl net-tools openssl screen tree
# Install meld if X server is installed - this way meld is not installed on headless systems
if dpkg-query -W -f='${Status}' xserver-xorg-core | grep -q -P '^install ok installed$'; then
    printf "\nInstalling meld\n"
    sudo apt-get install -y gedit meld
fi

# Required for the 'extract' bash function
sudo apt-get install -y -qq zip unzip p7zip-full

# Get the location of this script, no matter what directory it was run from, and its parent directory.
current_directory=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
parent_directory=$(dirname "$current_directory")
if [ ! -d "$parent_directory" ]; then
    echo "FAILED - Could not detect parent directory. Exiting."
    exit
fi

touch "$current_directory"/dotfiles/bash_extra

# TODO: Add an option to do this optionally or at least test the results in a headless setup
# Get solarized dircolors repository
cd "$parent_directory"
if [ -d dircolors-solarized/.git ]; then
    printf "\nPulling the solarized dircolors repo\n"
    cd dircolors-solarized
    git pull
else
    printf "\nCloning the solarized dircolors repo\n"
    git clone https://github.com/seebi/dircolors-solarized.git
fi

# Fill in the dircolors_solarized_path so that 'bash_prompt' dotfiles can find it
sed -i "s,^dircolors_solarized_path=.*\$,dircolors_solarized_path=\"$parent_directory/dircolors-solarized/\",g" "$current_directory/dotfiles/bash_prompt"
sed -i "s,^colors_file=.*\$,colors_file=\"dircolors.ansi-dark\",g" "$current_directory/dotfiles/bash_prompt"

# Creates a backup dir, create the symlinks, remove backup dir (if empty)
cd "$current_directory"
create_backup_directory
create_symlinks
remove_empty_backup

echo "REMINDER: Setup the .gitconfig [user] values."
echo 'git config user.name "<user name>"'
echo 'git config user.email "<email address>"'
echo "Set this globally with --global if desired."
