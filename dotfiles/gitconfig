[alias]
    # View abbreviated SHA, description, and history graph of the latest 20 commits
    l = log --pretty=oneline -n 20 --graph --abbrev-commit

    # View the current working tree status using the short format
    s = status -s

    # Show the diff using our 'difftool' (-y for 'Yes' for meld)
    difft = !"git difftool -d"

    # Show the diff between the latest commit and the current state
    diffd = !"git diff-index --quiet HEAD -- || clear; git --no-pager diff --patch-with-stat"

    # `git di $number` shows the diff between the state `$number` revisions ago and the current state
    diffi = !"d() { git diff --patch-with-stat HEAD~$1; }; git diff-index --quiet HEAD -- || clear; d"

    # Pull in remote changes for the current repository and all its submodules
    pull-all = "git pull --recurse-submodules"

    # Clone a repository including all submodules
    clone-all = clone --recursive

    # Commit all changes
    commit-all = !git add -A && git commit -av

    # Switch to a branch, creating it if necessary
    #go = "!f() { git checkout -b \"$1\" 2> /dev/null || git checkout \"$1\"; }; f"

    # Show verbose output about tags, branches or remotes
    tags = tag -l
    branches = branch -a
    remotes = remote -v

    # List aliases
    aliases = config --get-regexp alias

    # Amend the currently staged files to the latest commit
    amend = commit --amend --reuse-message=HEAD

    # Credit an author on the latest commit
    credit = "!f() { git commit --amend --author \"$1 <$2>\" -C HEAD; }; f"

    # Interactive rebase with the given number of latest commits
    rebase-interactive = "!r() { git rebase -i HEAD~$1; }; r"

    # Find branches containing commit
    find-branches = "!f() { git branch -a --contains $1; }; f"

    # Find tags containing commit
    find-tags = "!f() { git describe --always --contains $1; }; f"

    # Find commits by source code
    find-commits = "!f() { git log --pretty=format:'%C(yellow)%h  %Cblue%ad  %Creset%s%Cgreen  [%cn] %Cred%d' --decorate --date=short -S$1; }; f"

    # Find commits by commit message
    find-commits-via-message = "!f() { git log --pretty=format:'%C(yellow)%h  %Cblue%ad  %Creset%s%Cgreen  [%cn] %Cred%d' --decorate --date=short --grep=$1; }; f"

    # Remove branches that have already been merged with master
    # a.k.a. ‘delete merged’
    delete-merged-branches = "!git branch --merged | grep -v '\\*' | xargs -n 1 git branch -d"

    # List contributors with number of commits
    contributors = shortlog --summary --numbered

    # Merge GitHub pull request on top of the `master` branch
    # mpr = "!f() { \
    #     if [ $(printf \"%s\" \"$1\" | grep '^[0-9]\\+$' > /dev/null; printf $?) -eq 0 ]; then \
    #         git fetch origin refs/pull/$1/head:pr/$1 && \
    #         git rebase master pr/$1 && \
    #         git checkout master && \
    #         git merge pr/$1 && \
    #         git branch -D pr/$1 && \
    #         git commit --amend -m \"$(git log -1 --pretty=%B)\n\nCloses #$1.\"; \
    #     fi \
    # }; f"

[apply]
    # Detect whitespace errors when applying a patch
    whitespace = fix

[core]
    # Use custom `.gitignore` and `.gitattributes`
    excludesfile = ~/.gitignore
    attributesfile = ~/.gitattributes

    # Turn off a blank line at the EOF generating an error, use default for the others
    whitespace = -blank-at-eof
    tabwidth = 4

    # Make `git rebase` safer on OS X
    # More info: <http://www.git-tower.com/blog/make-git-rebase-safe-on-osx/>
    trustctime = false

    # Set the editor to 'gedit', eventhough $EDITOR is gedit
    editor = gedit --wait --new-window

[color]
    # Use colors in Git commands that are capable of colored output when
    # outputting to the terminal. (This is the default setting in Git ≥ 1.8.4.)
    ui = auto

[color "branch"]
    current = yellow reverse
    local = yellow
    remote = green

[color "diff"]
    meta = yellow bold
    frag = magenta bold # line info
    old = red # deletions
    new = green # additions

[color "status"]
    added = yellow
    changed = green
    untracked = cyan

[credential]
    helper = cache

[diff]
    # Detect copies as well as renames
    renames = copies
    tool = meld

[diff "bin"]
	# Use `hexdump` to diff binary files
	textconv = hexdump -v -C

[diff "exif"]
    textconv = exiftool

[diff "word"]
    textconv = catdoc

[diff "wordx"]
    textconv = docx2txt

[difftool "meld"]
    cmd = /usr/bin/meld \"$LOCAL\" \"$REMOTE\"

[difftool]
    prompt = false

[help]
    # Automatically correct and execute mistyped commands
    autocorrect = 1

[merge]
    # Include summaries of merged commits in newly created merge commit messages
    log = true
    tool = meld

[mergetool "meld"]
    trustExitCode = true
    cmd = /usr/bin/meld --merge --result=\"$MERGED\" \"$LOCAL\" \"$BASE\" \"$REMOTE\"

# URL shorthands
[url "git@github.com:"]
    insteadOf = "gh:"
    pushInsteadOf = "github:"
    pushInsteadOf = "git://github.com/"

[url "git://github.com/"]
    insteadOf = "github:"

[url "git@gist.github.com:"]
    insteadOf = "gst:"
    pushInsteadOf = "gist:"
    pushInsteadOf = "git://gist.github.com/"

[url "git://gist.github.com/"]
    insteadOf = "gist:"
