#!/usr/bin/env bash

#-------------------------------------------------------------
# Xtitle related function
#-------------------------------------------------------------
# Puts a fun title in the title bar of the terminal
xtitle()
{
    case "$TERM" in
    *term* | rxvt)
        echo -en "\e]0;$*\a" ;;
    *)  ;;
    esac
}
# Aliases that use xtitle
alias top='xtitle Processes on $HOSTNAME && top'
alias make='xtitle Making $(basename $PWD) ; make'

#-------------------------------------------------------------
# Man Pages
#-------------------------------------------------------------
# Need to fix that it doesn't change the title and then it breaks pman, etc
man() {
    for i ; do
        xtitle The "$(basename "$i"|tr -d '.[:digit:]')" manual
        command man -a "$i"
    done
}

# Open a man page in using ${BROWSER} or directly with
# man -H(firefox, google-chrome, chromium-browser) <command>
bman() {
    if [ "$BROWSER" ]; then
        for i ; do
            command man -H"${BROWSER}" "$i"
        done
    else
        echo "You need to export BROWSER to firefox, google-chrome, chromium-browser, or the browser of your choice"
        command man -Hfirefox "$@"
    fi
}

#-------------------------------------------------------------
# File & strings related functions:
#-------------------------------------------------------------

# Find a file with a pattern in name:
ff() {
    find . -type f -iname '*'"$*"'*' -ls ;
}

# Find a directory with a pattern in name:
fd() {
    find . -type d -iname '*'"$*"'*' -ls ;
}

# Find a file with pattern $1 in name and Execute $2 on it:
fe() {
    find . -type f -iname '*'"${1:-}"'*' -exec "${2:-file}" {} \;  ;
}

#  Find a pattern in a set of files and highlight them
fstr()
{
    OPTIND=1
    local mycase=""
    local usage="fstr: find string in files.
Usage: fstr [-i] \"pattern\" [\"filename pattern\"] "
    while getopts ":i" opt
    do
        case "$opt" in
           i) mycase="-i " ;;
           *) echo "$usage"; return ;;
        esac
    done
    shift $(( OPTIND-1 ))
    if [ "$#" -lt 1 ]; then
        echo "$usage"
        return;
    fi
    find . -type f -name "${2:-*}" -print0 | xargs -0 egrep --color=always -sn "${mycase}" "$1" 2>&- | more
}

# Determine size of a file or total size of a directory
fs() {
    if du -b /dev/null > /dev/null 2>&1; then
        local arg=-sbh;
    else
        local arg=-sh;
    fi

    if [[ -n "$*" ]]; then
        du $arg -- "$@";
    else
        du $arg .[^.]* ./*;
    fi;
}

# Pretty-print of 'df' output. Inspired by 'dfc' utility.
# usage mydf .
mydf()
{
    for fs ; do
        if [ ! -d "$fs" ]; then
          echo -e "$fs"" :No such file or directory" ; continue
        fi
        local info
        IFS=" " read -r -a info <<< "$(command df -P "$fs" | awk 'END{ print $2,$3,$5 }')"
        local free
        IFS=" " read -r -a free <<< "$(command df -Pkh "$fs" | awk 'END{ print $4 }')"
        local nbstars=$(( 20 * info[1] / info[0] ))
        local out="["
        for ((j=0;j<20;j++)); do
            if [ ${j} -lt ${nbstars} ]; then
               out=$out"*"
            else
               out=$out"-"
            fi
        done
        out=${info[2]}" $out] (${free[*]} free on $fs)"
        echo -e "$out"
    done
}

# Convert to lowercase
# usage lowercase <*.txt>
lowercase() {
    for i in "$@";
        do mv -f "$i" "$(echo "$i"| tr '[:upper:]' '[:lower:]')" &>/dev/null;
    done
}

# Convert to uppercase
# usage uppercase <*.txt>
uppercase() {
    for i in "$@";
        do mv -f "$i" "$(echo "$i"| tr '[:lower:]' '[:upper:]')" &>/dev/null;
    done
}

swap()
{
    # Swap 2 filenames around, if they exist (from Uzi's bashrc).
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
    [ ! -e "$1" ] && echo "swap: $1 does not exist" && return 1
    [ ! -e "$2" ] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

# `tre` is a shorthand for `tree` with hidden files and color enabled, ignoring
# the `.git` directory, listing directories first. The output gets piped into
# `less` with options to preserve color and line numbers, unless the output is
# small enough for one screen.
tre() {
    # need to install tree
    tree -aC -I '.git|node_modules|bower_components' --dirsfirst "$@" | less -FRNX;
}

# Handy Extract Program
extract()
{
    if [ -f "$1" ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf "$1"     ;;
            *.tar.gz)    tar xvzf "$1"     ;;
            *.bz2)       bunzip2 "$1"      ;;
            *.rar)       7z x "$1"      ;;
            *.gz)        gunzip "$1"       ;;
            *.tar)       tar xvf "$1"      ;;
            *.tbz2)      tar xvjf "$1"     ;;
            *.tgz)       tar xvzf "$1"     ;;
            *.zip)       unzip "$1"        ;;
            *.Z)         uncompress "$1"   ;;
            *.7z)        7z x "$1"         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# Creates an archive (*.tar.gz) from given directory.
maketar()
{
    tar cvzf "${1%%/}.tar.gz"  "${1%%/}/";
}

# Create a ZIP archive of a file or folder.
makezip() {
    zip -r "${1%%/}.zip" "$1";
}

# Make your directories and files access rights sane.
sanitize() {
    chmod -R u=rwX,g=rX,o= "$@";
}

#-------------------------------------------------------------
# Process/system related functions:
#-------------------------------------------------------------
myps() {
    ps "$@" -u "${USER}" -o "pid,%cpu,%mem,bsdtime,command";
}

mypp() {
    myps f | awk '!/awk/ && $0~var' var="${1:-".*"}";
}

# Kill by process name
killps()
{
    local pid pname sig="-TERM"   # default signal
    if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
        echo "Usage: killps [-SIGNAL] pattern"
        return;
    fi
    if [ $# = 2 ]; then sig=$1 ; fi
    for pid in $(myps| awk '!/awk/ && $0~pat { print $1 }' pat=${!#} )
    do
        pname=$(myps | awk '$1~var { print $5 }' var="$pid" )
        # Note - 'ask' is a local function defined a little further on
        if ask "Kill process $pid <$pname> with signal $sig?"
            then kill "$sig" "$pid"
        fi
    done
}

# Get IP address on ethernet
myip()
{
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' | sed -e s/addr://)
    echo "${MY_IP:-"Not connected"}"
}

# Get current host related info
ii()
{
    # Define colors if not defined
    if [ -z "${Red}" ]; then
        local Red='\e[0;31m'
    fi
    if [ -z "${No_Color}" ]; then
        local No_Color='\e[0m'
    fi

    echo -e "\nYou are logged on to: ${Red}${HOSTNAME}"
    echo -e "\n${Red}Additional information:${No_Color} " ; uname -a
    echo -e "\n${Red}Users logged on:${No_Color} " ; w -hi | cut -d " " -f1 | sort | uniq
    echo -e "\n${Red}Current date:${No_Color} " ; date
    echo -e "\n${Red}Machine stats:${No_Color} " ; uptime
    echo -e "\n${Red}Memory stats:${No_Color} " ; free
    echo -e "\n${Red}Diskspace:${No_Color} " ; mydf / "${HOME}"
    echo -e "\n${Red}Local IP Address:${No_Color}" ; myip
    echo -e "\n${Red}Open connections:${No_Color} "; netstat -pan --inet;
    echo
}

#-------------------------------------------------------------
# Misc utilities:
#-------------------------------------------------------------
alert() {
    # We will use either a terminal or error icon in the alert based on the exit value of the last command.
    # The call to 'local temp_HISTTIMEFORMAT=${HISTTIMEFORMAT};' will overwrite it, so be sure to save its value first
    local last_command_exit_value=$?
    # We have to unset HISTTIMEFORMAT b/c the 'echo' statements for color mess up
    # the text in the alert box
    local temp_HISTTIMEFORMAT=${HISTTIMEFORMAT};
    unset HISTTIMEFORMAT;
    if [ "$last_command_exit_value" -eq 0 ]; then
        notify-send --urgency=low -i "terminal" "$(history|tail -n1|sed -e 's/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//')";
    else
        notify-send --urgency=low -i "error" "$(history|tail -n1|sed -e 's/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//')";
    fi
    export HISTTIMEFORMAT=${temp_HISTTIMEFORMAT}
}

# Repeat a command 'n' times
repeat()
{
    local i max
    max=$1; shift;
    for ((i=1; i <= max ; i++)); do  # --> C-like syntax
        eval "$@";
    done
}

# Prompt user for y/n
function ask()
{
    echo -n "$@" '[y/n] ' ; read -r ans
    case "$ans" in
        y*|Y*) return 0 ;;
        *) return 1 ;;
    esac
}

# Get name of app that created a corefile
corename()
{
    for file ; do
        echo -n "$file" : ; gdb --core="$file" --batch | head -1
    done
}

# Decode \x{ABCD}-style Unicode escape sequences,
# ie unidecode "Fl\xfcgel" will output Flugel with an umlaut on the 'u'
unidecode() {
    perl -e "binmode(STDOUT, ':utf8'); print \"$*\"";
    # Print a newline unless we’re piping the output to another program
    if [ -t 1 ]; then
        echo ""; # newline
    fi;
}

# Get a character’s Unicode code point, ie codepoint @ = U+0040
codepoint() {
    perl -e "use utf8; print sprintf('U+%04X', ord(\"$*\"))";
    # Print a newline unless we’re piping the output to another program
    if [ -t 1 ]; then
        echo "";
    fi;
}

# Show all the names (CNs and SANs) listed in the SSL certificate for a given domain
getcertnames() {
    if [ -z "${1}" ]; then
        echo "ERROR: No domain specified.";
        return 1;
    fi;

    local domain="${1}";
    echo "Testing ${domain}…";
    echo "";

    local tmp
    tmp=$(echo -e "GET / HTTP/1.0\nEOT" \
        | openssl s_client -connect "${domain}:443" -servername "${domain}" 2>&1);

    if [[ "${tmp}" = *"-----BEGIN CERTIFICATE-----"* ]]; then
        local certText
        certText=$(echo "${tmp}" \
            | openssl x509 -text -certopt "no_aux, no_header, no_issuer, no_pubkey, \
            no_serial, no_sigdump, no_signame, no_validity, no_version");
        echo "Common Name:";
        echo "";
        echo "${certText}" | grep "Subject:" | sed -e "s/^.*CN=//" | sed -e "s/\/emailAddress=.*//";
        echo "";
        echo "Subject Alternative Name(s):";
        echo "";
        echo "${certText}" | grep -A 1 "Subject Alternative Name:" \
            | sed -e "2s/DNS://g" -e "s/ //g" | tr "," "\n" | tail -n +2;
        return 0;
    else
        echo "ERROR: Certificate not found.";
        return 1;
    fi;
}

# Show hidden files in Nautilus
showhidden() {
    # Check if we received no arguments
    if [ $# -eq 0 ]; then
        echo "No arguments supplied - reversing current value"
        local show_value
        show_value=$(gsettings get org.gtk.Settings.FileChooser show-hidden)
        # If we are showing hidden files, then stop
        if [ "${show_value}" = true ]; then
            gsettings set org.gtk.Settings.FileChooser show-hidden false
        else
            gsettings set org.gtk.Settings.FileChooser show-hidden true
        fi
    else
        gsettings set org.gtk.Settings.FileChooser show-hidden "$1"
    fi
}

# Remove __pycache__ and .pyc/.pyo files
pyclean () {
    find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
}
