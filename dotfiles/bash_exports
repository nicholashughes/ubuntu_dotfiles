#!/usr/bin/env bash

# Define colors if they are not defined
if [ -z "${Cyan}" ]; then
    Cyan='\e[0;36m'
fi
if [ -z "${Green}" ]; then
    Green='\e[0;32m'
fi
if [ -z "${No_Color}" ]; then
    No_Color='\e[0m'
fi
BRed='\e[1;31m'
White='\e[0;37m'
On_Blue='\e[44m'

# Setup for Bash --------------------------------------------------------------
# Bash Time options
export TIMEFORMAT=$'\nreal %3R\tuser %3U\tsys %3S\tpcpu %P\n'

# Bash History options
export HISTSIZE=32768
export HISTFILESIZE="${HISTSIZE}"
export HISTCONTROL=ignoredups
export HISTIGNORE="&:bg:fg:ll:h"
HISTTIMEFORMAT="$(echo -e "${Cyan}")[%d/%m %H:%M:%S]$(echo -e "${No_Color}") "
export HISTTIMEFORMAT

# Set the default editor
export EDITOR="gedit --wait --new-window"

# Ignore 1st EOF (ie ctrl+d twice to exit)
export ignoreeof=1

# Ignore words ending with these suffixes, when doing tab completion.
export FIGNORE='#:.pyc'

# Prefer US English and use UTF-8
: "${LANG:="en_US.UTF-8"}"
: "${LANGUAGE:="en_US.UTF-8"}"
: "${LC_CTYPE:="en_US.UTF-8"}"
: "${LC_ALL:="en_US.UTF-8"}"
export LANG LANGUAGE LC_CTYPE LC_ALL

# Setup for less --------------------------------------------------------------
alias more='less'
# Don’t clear the screen after quitting a manual page
export MANPAGER='less -X'
# LESS settings
export PAGER=${MANPAGER}
export LESSOPEN='|/usr/bin/lesspipe.sh %s 2>&-'
# When running less, always ignore case on searches; show line numbers; prompt verbosely; display colors; chop long lines; highlight first new line; tabs are 4 chars wide.
export LESS='-i -N -M -R -S -w -z-4 -X -F -P%t?f%f :stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'

# LESS man page colors
LESS_TERMCAP_mb=$(printf "${BRed}")
export LESS_TERMCAP_mb
LESS_TERMCAP_md=$(printf "${BRed}")
export LESS_TERMCAP_md
LESS_TERMCAP_me=$(printf "${No_Color}")
export LESS_TERMCAP_me
LESS_TERMCAP_se=$(printf "${No_Color}")
export LESS_TERMCAP_se
#LESS_TERMCAP_so=$(printf "${Yellow}${On_Blue}")
LESS_TERMCAP_so=$(printf "${White}${On_Blue}")
export LESS_TERMCAP_so
LESS_TERMCAP_ue=$(printf "${No_Color}")
export LESS_TERMCAP_ue
LESS_TERMCAP_us=$(printf "${Green}")
export LESS_TERMCAP_us

# Setup for pip ---------------------------------------------------------------
# Don't allow pip to run if there is no virtualenv currently activated
export PIP_REQUIRE_VIRTUALENV=true
# Override the pip virtualenv requirement
gpip() {
    PIP_REQUIRE_VIRTUALENV="" pip "$@"
}

# Setup for python ------------------------------------------------------------
export PYTHONSTARTUP="$HOME"/.pythonrc.py
export PYTHONDONTWRITEBYTECODE=1
