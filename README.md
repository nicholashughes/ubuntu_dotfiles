# Ubuntu Dotfiles
A collection of dotfiles for Ubuntu.

<a name="table-of-contents" id="table-of-contents"></a>
## Table of Contents
- [Quickstart](#quickstart)
- [Overview](#overview)
- [Dotfiles](#dotfiles)

<a name="quickstart" id="quickstart"></a>
## Quickstart
To install the dotfiles, run this script.
```
./install.sh
```
This will also create a backup of your dotfiles in `~/.dotfiles_backups/`.

To uninstall the dotfiles, run this script.
```
./remove.sh
```
This does not currently reset your dotfiles from your backup directory, rather it just unlinks any dotfiles that
were symlinked.

<a name="overview" id="overview"></a>
## Overview
This project configures a set of dotfiles for Ubuntu. They can be installed/removed via their respective scripts
(see the [Quickstart](#quickstart)) section.

#### Install
```
./install.sh
```
The installation actually sets a series of symlinks to the files within
this project. If an actual dotfile of that type exists (ie .bashrc), that file is backed up into a backup directory
located at `~/.dotfiles_backups/`. The original backup directory will be `~/.dotfiles_backups/orig/`, while subsequent
backups would be saved in `~/.dotfiles_backups/<timestamp>/`.

Note: This script will install some needed dependencies.

#### Uninstall
```
./remove.sh
```
Removing the dotfiles with the `remove.sh` script will
unlink any dotfiles that are linked in this directory. It does not restore the old dotfiles, currently this must be
done by hand.

<a name="dotfiles" id="dotfiles"></a>
## Dotfiles
A description of the dotfiles that are included within this project.

| File           | Description |
| -------------- | -------------------------------- |
| bash_aliases   | Contains a list of bash aliases |
| bash_exports   | Contains bash exports |
| bash_extra     | This is loaded at the end; used for final overwrites or temporary settings |
| bash_functions | Contains a list of bash functions |
| bash_logout    | Clears the screen when logging out |
| bash_profile   | Loads the dotfiles |
| bash_prompt    | Set the colors on the terminal prompt |
| bashrc         | This loads `.bash_profile` |
| curlrc         | Settings for `curl` |
| editorconfig   | Settings for `editorconfig` |
| gitattributes  | Declares how file types should be diff'd |
| gitconfig      | Sets git aliases and other options |
| gitignore      | Sets global files to ignore |
| gvimrc         | Settings for `gvim` |
| inputrc        | Set Readline options such as autocomplete |
| pythonrc.py    | Set options for the python shell |
| screenrc       | Settings for `screen` |
| vimrc          | Settings for `vim` |
| vim/           | Settings for `vim` backups, colors, swaps, syntax, undo |
| wgetrc         | Settings for `wget` |

